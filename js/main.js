$('.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    items: 1,
    navText: ["<div class='left'><span class='arrow-left'></span></div>", "<div class='right'><span class='arrow-right'></span></div>"],
    responsive: {
        0: {
            stagePadding: 20
        },
        769: {
            stagePadding: 0
        }
    }
})


// Google Map
function initMap() {
    var location = { lat: 41.314531, lng: 19.815672 };
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 14,
        center: location,
        styles: [
            {
                "featureType": "administrative",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text.fill",
                "stylers": [
                    {
                        "color": "#747474"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#f2f2f2"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#f9f9f9"
                    }
                ]
            },
            {
                "featureType": "landscape.natural.terrain",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "lightness": "0"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "saturation": "-100"
                    },
                    {
                        "gamma": "1.00"
                    },
                    {
                        "lightness": "41"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "lightness": "0"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "lightness": "-7"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [
                    {
                        "lightness": "-9"
                    },
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "labels",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "visibility": "off"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#e4e4e4"
                    },
                    {
                        "visibility": "on"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "lightness": "-3"
                    }
                ]
            }
        ]
    });

    var contentString =
        '<div id="content">' +
        '<div id="inner-content" class="text-center">' +
        '<h6>GOOD TIMES</h6>' +
        '<p>12 Yonge Street, Toronto, ON, Canada</p>' +
        '</div>' +
        '</div>';

    var infoWindow = new google.maps.InfoWindow({
        content: contentString
    });

    var marker = new google.maps.Marker({
        position: location,
        map: map,
        icon: 'images/marker.png',
        infoWindow: infoWindow
    })

    google.maps.event.addListener(marker, 'click', function () {
        infoWindow.open(map, marker);
    });
}




// Opening Hours
$(document).ready(function () {
    function tConvert(time) {
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
        if (time.length > 1) {
            time = time.slice(1);
            time[5] = +time[0] < 12 ? 'AM' : 'PM';
            time[0] = +time[0] % 12 || 12;
        }
        return time.join('');
    }

    var data = JSON.parse($('#data').html());

    var d = new Date();

    var weekday = new Array(7);
    weekday[0] = "Sunday";
    weekday[1] = "Monday";
    weekday[2] = "Tuesday";
    weekday[3] = "Wednesday";
    weekday[4] = "Thursday";
    weekday[5] = "Friday";
    weekday[6] = "Saturday";

    data.openingHoursSpecification.map(openingsItem => {
        if (parseInt(openingsItem.closes) > d.getHours()) {
            document.getElementById('open-text').textContent = 'Open Today';
            document.getElementById('mobile-open-text').textContent = 'Open Today';
        } else {
            var i = 0, j = 0;
            if (!openingsItem.is_open) {
                openingsItem.dayOfWeek.map(item => {
                    i = weekday.indexOf(item);
                });
                j = (i - d.getDay()) + 1;
                document.getElementById('open-text').textContent = `Opens ${weekday[d.getDay() + j]}`;
                document.getElementById('mobile-open-text').textContent = `Opens ${weekday[d.getDay() + j]}`;
            } else {
                document.getElementById('open-text').textContent = `Opens ${weekday[d.getDay() + 1]}`;
                document.getElementById('mobile-open-text').textContent = `Opens ${weekday[d.getDay() + 1]}`;
            }
        }

        document.getElementById('times').innerHTML = `${tConvert(openingsItem.opens)} - ${tConvert(openingsItem.closes)}`;
        document.getElementById('mobile-times').innerHTML = `${tConvert(openingsItem.opens)} - ${tConvert(openingsItem.closes)}`;
    });
});



// Helper function to get form data in the supported format
function getFormDataString(formEl) {
    var formData = new FormData(formEl),
        data = [];

    for (var keyValue of formData) {
        data.push(encodeURIComponent(keyValue[0]) + "=" + encodeURIComponent(keyValue[1]));
    }

    return data.join("&");
}

// Fetch the form element
var formEl = document.getElementById("book-form");

// Override the submit event
formEl.addEventListener("submit", function (e) {
    e.preventDefault();

    var request = new XMLHttpRequest();

    request.addEventListener("load", function () {
        if (request.status === 302) { // CloudCannon redirects on success
            // It worked
        }
    });

    request.open(formEl.method, formEl.action);
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    request.send(getFormDataString(formEl));
    formEl.style.display = 'none';
    document.getElementById('alert').style.display = 'block';
});